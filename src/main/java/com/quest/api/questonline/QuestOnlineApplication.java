package com.quest.api.questonline;

import com.quest.api.questonline.alternativa.model.Alternativa;
import com.quest.api.questonline.alternativa.repository.IAlternativaRepository;
import com.quest.api.questonline.pergunta.model.Pergunta;
import com.quest.api.questonline.pergunta.model.TemaPergunta;
import com.quest.api.questonline.pergunta.repository.IPerguntaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class QuestOnlineApplication implements CommandLineRunner {

	@Autowired
	private IPerguntaRepository perguntaRepository;

	@Autowired
	private IAlternativaRepository alternativaRepository;


	public static void main(String[] args) {
		SpringApplication.run(QuestOnlineApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		if (perguntaRepository.findAll() != null){
			perguntaRepository.deleteAll();
		}

		Pergunta p1 = new Pergunta(
				null,
				"Qual o formato do planeta Terra, o planeta em que vivemos?",
				TemaPergunta.CIENCIA
		);

		Pergunta p2 = new Pergunta(
				null,
				"Donatello era um famoso?",
				TemaPergunta.ARTE
		);

		Pergunta p3 = new Pergunta(
				null,
				"Um jogo oficial de fotebol é dividido em duas partes de quantos minutos?",
				TemaPergunta.ESPORTE
		);

		Pergunta p4 = new Pergunta(
				null,
				"Qual destas não é uma das 7 maravilhas do mundo?",
				TemaPergunta.MUNDO
		);

		Pergunta p5 = new Pergunta(
				null,
				"Chilli Beans é uma franquia famosa por qual produto?",
				TemaPergunta.SOCIEDADE
		);

		Pergunta p6 = new Pergunta(
				null,
				"O Bóson de Higgs é uma partícula sub-atômica téorica que seria responsável por dar " +
						"massa a porpos. Essa idéia foi teorizada por qual cientista?",
				TemaPergunta.CIENCIA
		);

		Alternativa a1p1 = new Alternativa(
				null,
				"Plana",
				p1
		);

		Alternativa a2p1 = new Alternativa(
				null,
				"Esférica",
				p1
		);

		Alternativa a3p1 = new Alternativa(
				null,
				"Elipsóide",
				p1
		);

		Alternativa a4p1 = new Alternativa(
				null,
				"Cúbica",
				p1
		);

		Alternativa a5p1 = new Alternativa(
				null,
				"Tetraédrica",
				p1
		);

		Alternativa a1p2 = new Alternativa(
				null,
				"Tartaruga",
				p2
		);

		Alternativa a2p2 = new Alternativa(
				null,
				"Atleta",
				p2
		);

		Alternativa a3p2 = new Alternativa(
				null,
				"Astro do Rock",
				p2
		);

		Alternativa a4p2 = new Alternativa(
				null,
				"Político",
				p2
		);

		Alternativa a5p2 = new Alternativa(
				null,
				"Escultor",
				p2
		);

		Alternativa a1p3 = new Alternativa(
				null,
				"1",
				p3
		);

		Alternativa a2p3 = new Alternativa(
				null,
				"2",
				p3
		);

		Alternativa a3p3 = new Alternativa(
				null,
				"3",
				p3
		);

		Alternativa a4p3 = new Alternativa(
				null,
				"4",
				p3
		);

		Alternativa a5p3 = new Alternativa(
				null,
				"5",
				p3
		);

		Alternativa a1p4 = new Alternativa(
				null,
				"Coliseu de Roma",
				p4
		);

		Alternativa a2p4 = new Alternativa(
				null,
				"Cristo Redentor",
				p4
		);

		Alternativa a3p4 = new Alternativa(
				null,
				"Grande Muralha da China",
				p4
		);

		Alternativa a4p4 = new Alternativa(
				null,
				"Machu Picchu",
				p4
		);

		Alternativa a5p4 = new Alternativa(
				null,
				"Torre Eiffel",
				p4
		);

		Alternativa a1p5 = new Alternativa(
				null,
				"Cocaina",
				p5
		);

		Alternativa a2p5 = new Alternativa(
				null,
				"Óculos",
				p5
		);

		Alternativa a3p5 = new Alternativa(
				null,
				"Pimenta",
				p5
		);

		Alternativa a4p5 = new Alternativa(
				null,
				"Preservativos",
				p5
		);

		Alternativa a5p5 = new Alternativa(
				null,
				"Livros",
				p5
		);

		Alternativa a1p6 = new Alternativa(
				null,
				"Peter Higgs",
				p6
		);

		Alternativa a2p6 = new Alternativa(
				null,
				"Albert Einstein",
				p6
		);

		Alternativa a3p6 = new Alternativa(
				null,
				"Linus Pauling",
				p6
		);

		Alternativa a4p6 = new Alternativa(
				null,
				"Max Planck",
				p6
		);

		Alternativa a5p6 = new Alternativa(
				null,
				"Galileu Galilei",
				p6
		);


		p1.getAlternativas().addAll(
				Arrays.asList(a1p1, a2p1, a3p1, a4p1, a5p1));

		p2.getAlternativas().addAll(
				Arrays.asList(a1p2, a2p2, a3p2, a4p2, a5p2));

		p3.getAlternativas().addAll(
				Arrays.asList(a1p3, a2p3, a3p3, a4p3, a5p3));

		p4.getAlternativas().addAll(
				Arrays.asList(a1p4, a2p4, a3p4, a4p4, a5p4));

		p5.getAlternativas().addAll(
				Arrays.asList(a1p5, a2p5, a3p5, a4p5, a5p5));

		p6.getAlternativas().addAll(
				Arrays.asList(a1p6, a2p6, a3p6, a4p6, a5p6));

		perguntaRepository.saveAll(Arrays.asList(p1,p2,p3,p4,p5,p6));

		alternativaRepository.saveAll(Arrays.asList(
				a1p1, a2p1, a3p1, a4p1, a5p1,
				a1p2, a2p2, a3p2, a4p2, a5p2,
				a1p3, a2p3, a3p3, a4p3, a4p3,
				a1p4, a2p4, a3p4, a4p4, a4p4,
				a1p5, a2p5, a3p5, a4p5, a4p5,
				a1p6, a2p6, a3p6, a4p6, a5p6
		));

	}
}
