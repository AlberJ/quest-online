package com.quest.api.questonline.alternativa.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.quest.api.questonline.pergunta.model.Pergunta;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Alternativa implements Serializable {

    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String texto;

    @JsonBackReference
    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "pergunta_id", nullable = false)
    private Pergunta pergunta;

    public Alternativa() {}

    public Alternativa(String texto, Pergunta pergunta) {
        this.texto = texto;
        this.pergunta = pergunta;
    }

    public Alternativa(Long id, String texto, Pergunta pergunta) {
        this.id = id;
        this.texto = texto;
        this.pergunta = pergunta;
    }

    public static Long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Pergunta getPergunta() {
        return pergunta;
    }

    public void setPergunta(Pergunta pergunta) {
        this.pergunta = pergunta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Alternativa that = (Alternativa) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
