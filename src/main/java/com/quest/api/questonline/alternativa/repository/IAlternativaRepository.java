package com.quest.api.questonline.alternativa.repository;

import com.quest.api.questonline.alternativa.model.Alternativa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IAlternativaRepository extends JpaRepository<Alternativa, Long> {
}
