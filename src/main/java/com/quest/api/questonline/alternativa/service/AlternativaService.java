package com.quest.api.questonline.alternativa.service;

import com.quest.api.questonline.alternativa.model.Alternativa;
import com.quest.api.questonline.alternativa.repository.IAlternativaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AlternativaService {

    @Autowired
    private IAlternativaRepository repository;

    public Alternativa create(Alternativa a){
        return repository.save(a);
    }

    public List<Alternativa> getAll(){
        return repository.findAll();
    }

    public Optional<Alternativa> find(Long id){
        return repository.findById(id);
    }

    public Alternativa update(long id, Alternativa a){
        Optional<Alternativa> alternativa = repository.findById(id);

        if(alternativa.isPresent()){
            a.setId(id);
            return repository.save(a);
        }

        return null;
    }

    public Alternativa delete(Long id){
        Optional<Alternativa> alternativa = repository.findById(id);
        if (alternativa.isPresent()){
            repository.deleteById(id);
            return alternativa.get();
        }

        return null;
    }
}
