package com.quest.api.questonline.pergunta.DTO;

import com.quest.api.questonline.pergunta.model.Pergunta;

import java.io.Serializable;

public class PerguntaDTO implements Serializable {
    private static final Long serialVersionUID = 1L;

    private Long id;
    private String enuncicado;
    private String tema;

    public PerguntaDTO() {
    }

    public PerguntaDTO(Pergunta pergunta) {
        this.id = pergunta.getId();
        this.enuncicado = pergunta.getEnunciado();
        this.tema = pergunta.getTema().getDescricao();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEnuncicado() {
        return enuncicado;
    }

    public void setEnuncicado(String enuncicado) {
        this.enuncicado = enuncicado;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }
}
