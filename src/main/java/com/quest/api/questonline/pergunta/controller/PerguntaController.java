package com.quest.api.questonline.pergunta.controller;

import com.quest.api.questonline.pergunta.DTO.PerguntaDTO;
import com.quest.api.questonline.pergunta.model.Pergunta;
import com.quest.api.questonline.pergunta.service.PerguntaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/perguntas")
public class PerguntaController {

    @Autowired
    private PerguntaService service;

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> find(@PathVariable Long id){
        Optional<Pergunta> p = service.find(id);
        if (p.isPresent())
            return ResponseEntity.ok(p.get());

        return ResponseEntity.notFound().build();
    }

    @GetMapping()
    public ResponseEntity<List<PerguntaDTO>> getAll(){
        List<Pergunta> perguntas = service.getAll();
        List<PerguntaDTO> lista = perguntas.stream().map(PerguntaDTO::new).collect(Collectors.toList());
        return ResponseEntity.ok().body(lista);
    }

    @PostMapping()
    public ResponseEntity<?> create(@Valid @RequestBody Pergunta p){
        Pergunta novaPergunta = service.create(p);
        if(novaPergunta != null)
            return ResponseEntity.status(HttpStatus.CREATED).body(novaPergunta);

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(p);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id){
        Pergunta p = service.delete(id);
        return ResponseEntity.ok(p);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<?> update(
            @PathVariable Long id,
            @Valid @RequestBody Pergunta p
    )
    {
        Pergunta pergunta = service.update(id, p);

        if (pergunta == null)
            return ResponseEntity.notFound().build();

        return ResponseEntity.ok(pergunta);
    }

}
