package com.quest.api.questonline.pergunta.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.quest.api.questonline.alternativa.model.Alternativa;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Entity
public class Pergunta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @NotBlank
    private String enunciado;

    @NotNull
    private Integer tema;

    @JsonManagedReference
    @OneToMany(mappedBy = "pergunta")
    private List<Alternativa> alternativas = new ArrayList<>();

    public Pergunta() {
    }

    public Pergunta(Long id, String enunciado, TemaPergunta tema) {
        this.id = id;
        this.enunciado = enunciado;
        this.tema = tema.getCod();
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEnunciado() {
        return enunciado;
    }

    public void setEnunciado(String enunciado) {
        this.enunciado = enunciado;
    }

    public TemaPergunta getTema() {
        return TemaPergunta.toEnum(tema);
    }

    public void setTema(TemaPergunta tema) {
        this.tema = tema.getCod();
    }

    public List<Alternativa> getAlternativas() {
        return alternativas;
    }

    public void setAlternativas(List<Alternativa> alternativas) {
        this.alternativas = alternativas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pergunta pergunta = (Pergunta) o;
        return id.equals(pergunta.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
