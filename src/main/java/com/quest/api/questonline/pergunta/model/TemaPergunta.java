package com.quest.api.questonline.pergunta.model;

public enum TemaPergunta {

    SOCIEDADE (1, "Sociedade"),
    ARTE (2, "Arte e lazer"),
    ESPORTE (3, "Esporte e entretenimento"),
    CIENCIA (4, "Ciência e Tecnologia"),
    MUNDO (5, "Mundo");

    private int cod;
    private String descricao;

    TemaPergunta(int cod, String descricao) {
        this.cod = cod;
        this.descricao = descricao;
    }

    public int getCod() {
        return cod;
    }

    public String getDescricao() {
        return descricao;
    }

    public static TemaPergunta toEnum(Integer cod){
        if (cod == null)
            return null;

        for (TemaPergunta t : TemaPergunta.values()){
            if (cod.equals(t.getCod())){
                return t;
            }
        }

        throw new IllegalArgumentException("Id inválido: " + cod);
    }
}
