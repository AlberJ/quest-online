package com.quest.api.questonline.pergunta.repository;

import com.quest.api.questonline.pergunta.model.Pergunta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPerguntaRepository extends JpaRepository<Pergunta, Long> {
}
