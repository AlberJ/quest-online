package com.quest.api.questonline.pergunta.service;

import com.quest.api.questonline.pergunta.model.Pergunta;
import com.quest.api.questonline.pergunta.repository.IPerguntaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PerguntaService {

    @Autowired
    private IPerguntaRepository repository;

    public Optional<Pergunta> find(Long id){
        return repository.findById(id);
    }

    public Pergunta create(Pergunta p){
        return repository.save(p);
    }

    public List<Pergunta> getAll(){
        return repository.findAll();
    }

    public Pergunta delete(Long id){
        Optional<Pergunta> op = repository.findById(id);
        if (op.isPresent()){
            repository.deleteById(id);
            return op.get();
        }
        return null;
    }

    public Pergunta update(Long id, Pergunta p){
        Optional<Pergunta> pergunta = repository.findById(id);

        if (pergunta.isPresent()){
            p.setId(id);
            return repository.save(p);
        }

        return null;
    }
}
